<?php 
/*
Plugin Name: Woo Login Redirect
Version:0.1
Description: Redirect plugin for Login Module for Wordpress
Author: Vishnu Raj
Plugin URI:   https://synamaticsmediastudio.com
Author URI:   https://synamaticsmediastudio.com/
License:      GPL2
License URI:  https://www.gnu.org/licenses/gpl-2.0.html
Text Domain:  wporg
Domain Path:  /
*/

$plugin_identification = "wp_login_redirect_module";
global $plugin_identification;
// Registration and Database 
register_activation_hook( __FILE__, 'pluginprefix_function_to_run' );


/**
* Redirect users to custom URL based on their role after login
*
* @param string $redirect
* @param object $user
* @return string
*/
function wc_custom_user_redirect( $redirect, $user ) {
    $optionsList    = get_option( $GLOBALS['plugin_identification']);
    $role           = $user->roles[0];
    $redirect       = get_permalink( wc_get_page_id( 'shop' ) );

    foreach ($optionsList as $key => $value) {
        if($key == $role){
            $redirect = $value;
        }
    }
    return $redirect;
}
add_filter( 'woocommerce_login_redirect', 'wc_custom_user_redirect', 10, 2 ); 


function get_editable_rsoles() {
    global $wp_roles;
    $all_roles = $wp_roles->roles;
    $editable_roles = apply_filters('roles', $all_roles);
    return $editable_roles;
}

function login_redirect_admin_setup() {
    if(!get_option( $GLOBALS['plugin_identification'])){
        add_option( $GLOBALS['plugin_identification'], []);
    }
    if(isset($_POST['cs'])){
        $ds = [];
        foreach ($_POST as $key => $value) {
           $ds[$key]  = $value;
        }
        update_option( $GLOBALS['plugin_identification'], $ds );
    }
    include(__DIR__.'/views/index.php');
}

function login_redirect_admin_actions() {
    $user = wp_get_current_user();
    if ( in_array( 'administrator', (array) $user->roles ) ) {
        add_options_page("Login Redirect", "Redirection", 1, "login_redirect", "login_redirect_admin_setup",10,2);
    }
}
 
add_action('admin_menu', 'login_redirect_admin_actions');