<?php 
$data = [];
$optionsList = get_option( $GLOBALS['plugin_identification']);
foreach (get_editable_rsoles() as $key =>$value) {
    $data[$key] = $value['name'];
}


?>
<div class="container">
    <div class="header">
        <h3>Login Redirect Module</h3>
        <p>Select Login redirect for all types of users.</p>
    </div>
    <?php 
        $message = ($_POST['cs']) ?  "<div class=\"notice notice-success is-dismissible\"><p>Update Succeed.</p></div>":"";
        echo $message;
    ?>
</div>
<div class="container">
    <form action="" method="POST">
    <input type="hidden" name="cs" value="true">
    <table class="widefat fixed" cellspacing="0">
    <tbody>
        <?php foreach ($data as $key => $value) {?>
        <tr>
            <th><label for="<?php echo $key;?>"><?php echo $value;?></label></th>
            <td><input type="text" name="<?php echo $key;?>" id="<?php echo $key;?>" value="<?php echo $optionsList[$key];?>"></td>
        </tr>
        <?php }?>
    </tbody>
    <tfoot>
    <tr>
        <th colspan="2">
            <button class="button components-button editor-post-publish-button is-button is-default is-primary is-large" type="submit">Save</button>
        </th>
    </tr>
    </tfoot>
    </table>
    </form>
</div>
